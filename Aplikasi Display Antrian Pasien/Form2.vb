﻿Public Class Form2
    Sub Panggil_L4()
        Dim arrJumlahKarakterSpasi() As String
        Dim file As String

        file = String.Format("{0}\assets\sound\{1}.wav", Application.StartupPath(), "start_sign")
        My.Computer.Audio.Play(file, AudioPlayMode.WaitToComplete)

        file = String.Format("{0}\assets\sound\{1}.wav", Application.StartupPath(), "antrian")
        My.Computer.Audio.Play(file, AudioPlayMode.WaitToComplete)

        arrJumlahKarakterSpasi = Split(Label3.Text, " ")
        For i = LBound(arrJumlahKarakterSpasi) To UBound(arrJumlahKarakterSpasi)
            If arrJumlahKarakterSpasi(i).ToString <> "" Then
                file = String.Format("{0}\assets\sound\{1}.wav", Application.StartupPath(), arrJumlahKarakterSpasi(i).ToString.ToLower)
                My.Computer.Audio.Play(file, AudioPlayMode.WaitToComplete)
            End If
        Next

        file = String.Format("{0}\assets\sound\{1}.wav", Application.StartupPath(), "end_sign")
        My.Computer.Audio.Play(file, AudioPlayMode.WaitToComplete)
    End Sub
    Dim hasil As String
    Private Function Terbilang(ByVal x As Integer) As String
        Dim tampung As Double
        Dim teks As String
        Dim bagian As String
        Dim i As Integer
        Dim tanda As Boolean

        Dim letak(5)
        letak(1) = "RIBU "
        letak(2) = "JUTA "
        letak(3) = "MILYAR "
        letak(4) = "TRILYUN "

        If (x < 0) Then
            Terbilang = ""
            Exit Function
        End If

        If (x = 0) Then
            Terbilang = "NOL"
            Exit Function
        End If

        If (x < 2000) Then
            tanda = True
        End If
        teks = ""

        If (x >= 1.0E+15) Then
            Terbilang = "NILAI TERLALU BESAR"
            Exit Function
        End If

        For i = 4 To 1 Step -1
            tampung = Int(x / (10 ^ (3 * i)))
            If (tampung > 0) Then
                bagian = ratusan(tampung, tanda)
                teks = teks & bagian & letak(i)
            End If
            x = x - tampung * (10 ^ (3 * i))
        Next

        Terbilang = teks & ratusan(x, False)
    End Function
    Function ratusan(ByVal y As Double, ByVal flag As Boolean) As String
        Dim tmp As Double
        Dim bilang As String
        Dim bag As String
        Dim j As Integer

        Dim angka(9)
        angka(1) = "SE"
        angka(2) = "DUA "
        angka(3) = "TIGA "
        angka(4) = "EMPAT "
        angka(5) = "LIMA "
        angka(6) = "ENAM "
        angka(7) = "TUJUH "
        angka(8) = "DELAPAN "
        angka(9) = "SEMBILAN "

        Dim posisi(2)
        posisi(1) = "PULUH "
        posisi(2) = "RATUS "

        bilang = ""
        For j = 2 To 1 Step -1
            tmp = Int(y / (10 ^ j))
            If (tmp > 0) Then
                bag = angka(tmp)
                If (j = 1 And tmp = 1) Then
                    y = y - tmp * 10 ^ j
                    If (y >= 1) Then
                        posisi(j) = "BELAS "
                    Else
                        angka(y) = "SE"
                    End If
                    bilang = bilang & angka(y) & posisi(j)
                    ratusan = bilang
                    Exit Function
                Else
                    bilang = bilang & bag & posisi(j)
                End If
            End If
            y = y - tmp * 10 ^ j
        Next

        If (flag = False) Then
            angka(1) = "SATU "
        End If
        bilang = bilang & angka(y)
        ratusan = bilang
    End Function
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        hasil = ""

        Dim value As String = TextBox1.Text
        Dim i As Integer = CInt(value.Substring(1, value.Length - 1))

        hasil &= value.Substring(0, 1) & " " & Terbilang(i)

        Label3.Text = hasil
        Call Panggil_L4()
    End Sub
End Class