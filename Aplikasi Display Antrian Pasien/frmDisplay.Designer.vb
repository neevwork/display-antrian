﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frnDisplay
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frnDisplay))
        Me.lblCurrentQueue = New System.Windows.Forms.Label()
        Me.lblCurrentPoli = New System.Windows.Forms.Label()
        Me.lblQueueGigi = New System.Windows.Forms.Label()
        Me.lblQueueKIA = New System.Windows.Forms.Label()
        Me.lblQueueUmum = New System.Windows.Forms.Label()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.BackgroundWorker2 = New System.ComponentModel.BackgroundWorker()
        Me.lblInstansiAlamat = New System.Windows.Forms.Label()
        Me.lblInstansiNama = New System.Windows.Forms.Label()
        Me.BackgroundWorker3 = New System.ComponentModel.BackgroundWorker()
        Me.picClose = New System.Windows.Forms.PictureBox()
        Me.picLogo = New System.Windows.Forms.PictureBox()
        Me.picQueue = New System.Windows.Forms.PictureBox()
        Me.lblQueuePendaftaran = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.AxWindowsMediaPlayer1 = New AxWMPLib.AxWindowsMediaPlayer()
        CType(Me.picClose, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picQueue, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AxWindowsMediaPlayer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblCurrentQueue
        '
        Me.lblCurrentQueue.BackColor = System.Drawing.Color.White
        Me.lblCurrentQueue.Font = New System.Drawing.Font("Microsoft Sans Serif", 102.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrentQueue.Location = New System.Drawing.Point(113, 266)
        Me.lblCurrentQueue.Name = "lblCurrentQueue"
        Me.lblCurrentQueue.Size = New System.Drawing.Size(482, 246)
        Me.lblCurrentQueue.TabIndex = 1
        Me.lblCurrentQueue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCurrentPoli
        '
        Me.lblCurrentPoli.BackColor = System.Drawing.Color.White
        Me.lblCurrentPoli.Font = New System.Drawing.Font("Microsoft Sans Serif", 102.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrentPoli.Location = New System.Drawing.Point(95, 879)
        Me.lblCurrentPoli.Name = "lblCurrentPoli"
        Me.lblCurrentPoli.Size = New System.Drawing.Size(43, 31)
        Me.lblCurrentPoli.TabIndex = 3
        Me.lblCurrentPoli.Text = "MENU"
        Me.lblCurrentPoli.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblCurrentPoli.Visible = False
        '
        'lblQueueGigi
        '
        Me.lblQueueGigi.BackColor = System.Drawing.Color.White
        Me.lblQueueGigi.Font = New System.Drawing.Font("Microsoft Sans Serif", 72.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQueueGigi.Location = New System.Drawing.Point(732, 857)
        Me.lblQueueGigi.Name = "lblQueueGigi"
        Me.lblQueueGigi.Size = New System.Drawing.Size(481, 103)
        Me.lblQueueGigi.TabIndex = 7
        Me.lblQueueGigi.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblQueueKIA
        '
        Me.lblQueueKIA.BackColor = System.Drawing.Color.White
        Me.lblQueueKIA.Font = New System.Drawing.Font("Microsoft Sans Serif", 72.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQueueKIA.Location = New System.Drawing.Point(732, 640)
        Me.lblQueueKIA.Name = "lblQueueKIA"
        Me.lblQueueKIA.Size = New System.Drawing.Size(481, 104)
        Me.lblQueueKIA.TabIndex = 8
        Me.lblQueueKIA.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblQueueUmum
        '
        Me.lblQueueUmum.BackColor = System.Drawing.Color.White
        Me.lblQueueUmum.Font = New System.Drawing.Font("Microsoft Sans Serif", 72.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQueueUmum.Location = New System.Drawing.Point(114, 862)
        Me.lblQueueUmum.Name = "lblQueueUmum"
        Me.lblQueueUmum.Size = New System.Drawing.Size(481, 93)
        Me.lblQueueUmum.TabIndex = 9
        Me.lblQueueUmum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BackgroundWorker1
        '
        '
        'BackgroundWorker2
        '
        '
        'lblInstansiAlamat
        '
        Me.lblInstansiAlamat.AutoSize = True
        Me.lblInstansiAlamat.Font = New System.Drawing.Font("Arial", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInstansiAlamat.ForeColor = System.Drawing.Color.White
        Me.lblInstansiAlamat.Location = New System.Drawing.Point(257, 80)
        Me.lblInstansiAlamat.Name = "lblInstansiAlamat"
        Me.lblInstansiAlamat.Size = New System.Drawing.Size(0, 32)
        Me.lblInstansiAlamat.TabIndex = 14
        Me.lblInstansiAlamat.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblInstansiNama
        '
        Me.lblInstansiNama.AutoSize = True
        Me.lblInstansiNama.Font = New System.Drawing.Font("Arial", 36.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInstansiNama.ForeColor = System.Drawing.Color.White
        Me.lblInstansiNama.Location = New System.Drawing.Point(257, 23)
        Me.lblInstansiNama.Name = "lblInstansiNama"
        Me.lblInstansiNama.Size = New System.Drawing.Size(0, 55)
        Me.lblInstansiNama.TabIndex = 13
        Me.lblInstansiNama.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'BackgroundWorker3
        '
        '
        'picClose
        '
        Me.picClose.Image = Global.Aplikasi_Display_Antrian_Pasien.My.Resources.Resources._exit
        Me.picClose.Location = New System.Drawing.Point(1186, 12)
        Me.picClose.Name = "picClose"
        Me.picClose.Size = New System.Drawing.Size(54, 54)
        Me.picClose.TabIndex = 15
        Me.picClose.TabStop = False
        '
        'picLogo
        '
        Me.picLogo.Location = New System.Drawing.Point(93, 12)
        Me.picLogo.Name = "picLogo"
        Me.picLogo.Size = New System.Drawing.Size(158, 134)
        Me.picLogo.TabIndex = 12
        Me.picLogo.TabStop = False
        '
        'picQueue
        '
        Me.picQueue.Image = Global.Aplikasi_Display_Antrian_Pasien.My.Resources.Resources.background
        Me.picQueue.Location = New System.Drawing.Point(93, 157)
        Me.picQueue.Name = "picQueue"
        Me.picQueue.Size = New System.Drawing.Size(1147, 816)
        Me.picQueue.TabIndex = 11
        Me.picQueue.TabStop = False
        '
        'lblQueuePendaftaran
        '
        Me.lblQueuePendaftaran.BackColor = System.Drawing.Color.White
        Me.lblQueuePendaftaran.Font = New System.Drawing.Font("Microsoft Sans Serif", 72.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQueuePendaftaran.Location = New System.Drawing.Point(114, 640)
        Me.lblQueuePendaftaran.Name = "lblQueuePendaftaran"
        Me.lblQueuePendaftaran.Size = New System.Drawing.Size(481, 104)
        Me.lblQueuePendaftaran.TabIndex = 16
        Me.lblQueuePendaftaran.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Timer1
        '
        Me.Timer1.Interval = 700
        '
        'AxWindowsMediaPlayer1
        '
        Me.AxWindowsMediaPlayer1.Enabled = True
        Me.AxWindowsMediaPlayer1.Location = New System.Drawing.Point(698, 157)
        Me.AxWindowsMediaPlayer1.Name = "AxWindowsMediaPlayer1"
        Me.AxWindowsMediaPlayer1.OcxState = CType(resources.GetObject("AxWindowsMediaPlayer1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.AxWindowsMediaPlayer1.Size = New System.Drawing.Size(532, 375)
        Me.AxWindowsMediaPlayer1.TabIndex = 17
        '
        'frnDisplay
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.SeaGreen
        Me.ClientSize = New System.Drawing.Size(1264, 985)
        Me.Controls.Add(Me.AxWindowsMediaPlayer1)
        Me.Controls.Add(Me.lblQueuePendaftaran)
        Me.Controls.Add(Me.picClose)
        Me.Controls.Add(Me.lblInstansiAlamat)
        Me.Controls.Add(Me.lblInstansiNama)
        Me.Controls.Add(Me.picLogo)
        Me.Controls.Add(Me.lblCurrentQueue)
        Me.Controls.Add(Me.lblQueueUmum)
        Me.Controls.Add(Me.lblQueueKIA)
        Me.Controls.Add(Me.lblQueueGigi)
        Me.Controls.Add(Me.picQueue)
        Me.Controls.Add(Me.lblCurrentPoli)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frnDisplay"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form2"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.picClose, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picLogo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picQueue, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AxWindowsMediaPlayer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblCurrentQueue As Label
    Friend WithEvents lblCurrentPoli As Label
    Friend WithEvents lblQueueGigi As Label
    Friend WithEvents lblQueueKIA As Label
    Friend WithEvents lblQueueUmum As Label
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents BackgroundWorker2 As System.ComponentModel.BackgroundWorker
    Friend WithEvents picQueue As System.Windows.Forms.PictureBox
    Friend WithEvents picLogo As System.Windows.Forms.PictureBox
    Friend WithEvents lblInstansiAlamat As System.Windows.Forms.Label
    Friend WithEvents lblInstansiNama As System.Windows.Forms.Label
    Friend WithEvents picClose As System.Windows.Forms.PictureBox
    Friend WithEvents BackgroundWorker3 As System.ComponentModel.BackgroundWorker
    Friend WithEvents lblQueuePendaftaran As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents AxWindowsMediaPlayer1 As AxWMPLib.AxWindowsMediaPlayer
End Class
