﻿Imports System.IO
Imports System.Net
Imports System.Net.Sockets
Imports Newtonsoft.Json.Linq
Imports Newtonsoft.Json

Public Class frnDisplay

    Dim tl As TcpListener
    Dim ns As NetworkStream
    Dim br As BinaryReader
    Dim soc As Socket
    Dim ds As New ds

    Dim M_POLITableAdapters As New dsTableAdapters.M_POLITableAdapter

    Dim noUrut, idPoli, aliasPoli, prefixPoli As String

    ' Variable untuk menampung gambar banner
    Dim listBanner As New ArrayList

    Private labelControl As Object()

    Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Try
            ' Baca file config
            Dim configText As String = clsConfig.Load()

            ' Tampung pada object json
            Dim jsonSettings As JObject = JObject.Parse(configText.Replace(Chr(10), Chr(13)))

            ' Simpan nilai file config ke variable property
            My.Settings.Item("NamaInstansi") = jsonSettings("Instansi")("Nama").ToString
            My.Settings.Item("AlamatInstansi") = jsonSettings("Instansi")("Alamat").ToString
            My.Settings.Item("LocalPort") = jsonSettings("Local")("Port").ToString
            My.Settings.Item("ConnectionString") = String.Format("Data Source={0};Initial Catalog={1};Persist Security Info=False;User ID=sa;Password=S3cr3t", jsonSettings("DB")("Host").ToString, jsonSettings("DB")("Name").ToString)

            ' Set logo path
            Dim logoPath As String = String.Format("{0}\assets\img\logo.png", Application.StartupPath())

            ' Tampilkan logo
            Dim bmp As New Bitmap(logoPath)
            picLogo.ImageLocation = (logoPath)
            picLogo.Height = bmp.Height
            picLogo.Width = bmp.Width
            picLogo.Load()

            ' Set value nama dan alamat instansi
            lblInstansiNama.Text = My.Settings.NamaInstansi
            lblInstansiAlamat.Text = My.Settings.AlamatInstansi

            labelControl = {lblQueueKIA, lblQueueUmum, lblQueueGigi}
        Catch ex As Exception
            MessageBox.Show(String.Format("Error on Form Load: {0}", ex.Message), "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End
        End Try
    End Sub

    Private Sub UpdateMasterPoli()
        Dim dt As New DataTable
        Dim configText As String = clsPoliConfig.Load()

        ' Tampung pada object json
        Dim jsonSettings As JObject = JObject.Parse(configText.Replace(Chr(10), Chr(13)))

        Dim token As JToken = jsonSettings.GetValue("PoliConfigurations")

        dt = JsonConvert.DeserializeObject(Of DataTable)(token.ToString())

        For Each row In ds.M_POLI
            Dim dr() As DataRow = Nothing
            Try
                dr = dt.Select(String.Format("IDPOLI='{0}'", row.IDPOLI))
            Catch ex As Exception
            End Try
            Dim display As String = ""
            Try
                display = dr(0)("DISPLAY")
            Catch ex As Exception
            End Try
            Dim prefix As String = ""
            Try
                prefix = dr(0)("PREFIX")
            Catch ex As Exception
            End Try
            Dim label As String = ""
            Try
                label = dr(0)("CONTROLDISPLAY")
            Catch ex As Exception
            End Try

            row("ALIAS") = display
            row("PREFIXANTRIAN") = prefix

            For Each lbl In labelControl
                If lbl.Name.ToString() = label Then
                    lbl.Tag = prefix
                    Exit For
                End If
            Next
        Next
        ds.M_POLI.AcceptChanges()
    End Sub

    Private Sub frmDisplay_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        Try
            If Screen.AllScreens.Length > 1 Then
                Me.Location = Screen.AllScreens(1).WorkingArea.Location
            End If

            AxWindowsMediaPlayer1.uiMode = "none"
            AxWindowsMediaPlayer1.settings.setMode("loop", True)
            AxWindowsMediaPlayer1.settings.volume = 0


            ' Tentukan path gambar yang digunakan sebagai banner
            Dim bannerPath As String = String.Format("{0}\assets\video\", Application.StartupPath())

            ' Baca file yang ada di path banner
            ' Kemudian disimpan di variable listBanner
            Dim di As New DirectoryInfo(bannerPath)
            Dim fiArr As FileInfo() = di.GetFiles()
            Dim fri As FileInfo

            Dim playlist = AxWindowsMediaPlayer1.playlistCollection.newPlaylist("MyPlaylist")

            For Each fri In fiArr
                'listBanner.Add(String.Format("{0}{1}", bannerPath, fri.Name))

                Dim media = AxWindowsMediaPlayer1.newMedia(String.Format("{0}{1}", bannerPath, fri.Name))
                playlist.appendItem(media)
            Next fri

            ' Jalankan slide show banner
            If playlist.count > 0 Then
                AxWindowsMediaPlayer1.currentPlaylist = playlist
                AxWindowsMediaPlayer1.Ctlcontrols.play()
            End If

            ' Untuk keperluan komunikasi tcp
            CheckForIllegalCrossThreadCalls = False

            ' Ambil data dari database untuk ditampung di datatable
            M_POLITableAdapters.Fill(ds.M_POLI)
            UpdateMasterPoli()

            ' Jalankan background worker
            'BackgroundWorker1.RunWorkerAsync()
            BackgroundWorker2.RunWorkerAsync()
        Catch ex As Exception
            MessageBox.Show(String.Format("Error on Form Load: {0}", ex.Message), "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End
        End Try
    End Sub

    ' Event untuk mengatur lokasi control
    ' Agar lebih dinamis
    Private Sub frmDisplay_Resize(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Resize
        Try
            'picCategory.Left = (Me.Size.Width / 2) + 20
            AxWindowsMediaPlayer1.Left = (Me.Size.Width / 2) + 10
            picQueue.Left = (Me.Size.Width / 2) - 610

            lblCurrentQueue.Left = picQueue.Left + 22
            lblCurrentPoli.Left = lblCurrentQueue.Left

            lblQueuePendaftaran.Left = lblCurrentQueue.Left
            lblQueueUmum.Left = lblCurrentQueue.Left
            lblQueueKIA.Left = (Me.Size.Width / 2) + 30
            lblQueueGigi.Left = lblQueueKIA.Left
            'lblQueueKIA.Left = picCategory.Left + 22
            'lblQueueUmum.Left = lblQueueKIA.Left
            'lblQueueGigi.Left = lblQueueKIA.Left

            picLogo.Left = picQueue.Left
            lblInstansiNama.Left = picLogo.Left + 110
            lblInstansiAlamat.Left = picLogo.Left + 115

            picClose.Left = Me.Size.Width - 80
        Catch ex As Exception
            MessageBox.Show(String.Format("Error on Sub New: {0}", ex.Message), "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub BackgroundWorker1_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
ResetConnection:
        tl = New TcpListener(IPAddress.Any, CInt(My.Settings.LocalPort))
        tl.Start()

        soc = tl.AcceptSocket

        ns = New NetworkStream(soc)
        br = New BinaryReader(ns)

        While True
            Try
                Dim value As String = br.ReadString
                Dim str() As String = value.Split(";")

                Console.WriteLine(value)

                Try
                    noUrut = str(0)
                Catch ex As Exception
                    noUrut = "-"
                End Try
                Try
                    Dim dr() As DataRow = ds.M_POLI.Select(String.Format("IDPOLI = '{0}'", str(1)))
                    If dr.Length > 0 Then
                        idPoli = dr(0)("IDPOLI")
                        aliasPoli = dr(0)("ALIAS")
                        prefixPoli = dr(0)("PREFIXANTRIAN")

                        For Each lbl In labelControl
                            If lbl.Tag = prefixPoli Then
                                lbl.Text = noUrut
                                Exit For
                            End If
                        Next

                        ' Dimatikan karena statis
                        'Select Case prefixPoli
                        '    Case "A"
                        '        lblQueueKIA.Text = noUrut
                        '    Case "B"
                        '        lblQueueUmum.Text = noUrut
                        '    Case "C"
                        '        lblQueueGigi.Text = noUrut
                        'End Select

                        'hasil = ""
                        Dim bilangan As Integer = CInt(noUrut.Substring(1, noUrut.Length - 1))

                        'hasil &= noUrut.Substring(0, 1) & " " & Terbilang(i)
                        hasil = noUrut.Substring(0, 1) & " " & Terbilang(bilangan)

                        strTerbilang = hasil

                        lblCurrentQueue.Text = noUrut
                        lblCurrentPoli.Text = aliasPoli

                        Call Panggil_L4()
                    Else
                        aliasPoli = "-"
                    End If
                Catch ex As Exception
                    aliasPoli = "-"
                End Try
                lblCurrentQueue.Text = noUrut
                lblCurrentPoli.Text = aliasPoli
            Catch ex As Exception
                soc.Close()
                tl.Stop()
                GoTo ResetConnection
            End Try
        End While
    End Sub

    Private Sub picClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles picClose.Click
        End
    End Sub

#Region "Sound"
    Dim strTerbilang As String
    Sub Panggil_L4()
        Dim arrJumlahKarakterSpasi() As String
        Dim file As String

        file = String.Format("{0}\assets\sound\{1}.wav", Application.StartupPath(), "start_sign")
        My.Computer.Audio.Play(file, AudioPlayMode.WaitToComplete)

        file = String.Format("{0}\assets\sound\{1}.wav", Application.StartupPath(), "antrian")
        My.Computer.Audio.Play(file, AudioPlayMode.WaitToComplete)

        arrJumlahKarakterSpasi = Split(strTerbilang, " ")
        For i = LBound(arrJumlahKarakterSpasi) To UBound(arrJumlahKarakterSpasi)
            If arrJumlahKarakterSpasi(i).ToString <> "" Then
                file = String.Format("{0}\assets\sound\{1}.wav", Application.StartupPath(), arrJumlahKarakterSpasi(i).ToString.ToLower)
                My.Computer.Audio.Play(file, AudioPlayMode.WaitToComplete)
            End If
        Next

        Select Case prefixPoli
            Case "A"
                file = String.Format("{0}\assets\sound\{1}.wav", Application.StartupPath(), "kia")
            Case "B"
                file = String.Format("{0}\assets\sound\{1}.wav", Application.StartupPath(), "umum")
            Case "C"
                file = String.Format("{0}\assets\sound\{1}.wav", Application.StartupPath(), "gigi")
            Case Else
                file = String.Format("{0}\assets\sound\{1}.wav", Application.StartupPath(), "pendaftaran")
        End Select
        My.Computer.Audio.Play(file, AudioPlayMode.WaitToComplete)

        file = String.Format("{0}\assets\sound\{1}.wav", Application.StartupPath(), "end_sign")
        My.Computer.Audio.Play(file, AudioPlayMode.WaitToComplete)
    End Sub
    Dim hasil As String
    Private Function Terbilang(ByVal x As Integer) As String
        Dim tampung As Double
        Dim teks As String
        Dim bagian As String
        Dim i As Integer
        Dim tanda As Boolean

        Dim letak(5)
        letak(1) = "RIBU "
        letak(2) = "JUTA "
        letak(3) = "MILYAR "
        letak(4) = "TRILYUN "

        If (x < 0) Then
            Terbilang = ""
            Exit Function
        End If

        If (x = 0) Then
            Terbilang = "NOL"
            Exit Function
        End If

        If (x < 2000) Then
            tanda = True
        End If
        teks = ""

        If (x >= 1.0E+15) Then
            Terbilang = "NILAI TERLALU BESAR"
            Exit Function
        End If

        For i = 4 To 1 Step -1
            tampung = Int(x / (10 ^ (3 * i)))
            If (tampung > 0) Then
                bagian = ratusan(tampung, tanda)
                teks = teks & bagian & letak(i)
            End If
            x = x - tampung * (10 ^ (3 * i))
        Next

        Terbilang = teks & ratusan(x, False)
    End Function
    Function ratusan(ByVal y As Double, ByVal flag As Boolean) As String
        Dim tmp As Double
        Dim bilang As String
        Dim bag As String
        Dim j As Integer

        Dim angka(9)
        angka(1) = "SE"
        angka(2) = "DUA "
        angka(3) = "TIGA "
        angka(4) = "EMPAT "
        angka(5) = "LIMA "
        angka(6) = "ENAM "
        angka(7) = "TUJUH "
        angka(8) = "DELAPAN "
        angka(9) = "SEMBILAN "

        Dim posisi(2)
        posisi(1) = "PULUH "
        posisi(2) = "RATUS "

        bilang = ""
        For j = 2 To 1 Step -1
            tmp = Int(y / (10 ^ j))
            If (tmp > 0) Then
                bag = angka(tmp)
                If (j = 1 And tmp = 1) Then
                    y = y - tmp * 10 ^ j
                    If (y >= 1) Then
                        posisi(j) = "BELAS "
                    Else
                        angka(y) = "SE"
                    End If
                    bilang = bilang & angka(y) & posisi(j)
                    ratusan = bilang
                    Exit Function
                Else
                    bilang = bilang & bag & posisi(j)
                End If
            End If
            y = y - tmp * 10 ^ j
        Next

        If (flag = False) Then
            angka(1) = "SATU "
        End If
        bilang = bilang & angka(y)
        ratusan = bilang
    End Function
#End Region

    Private Sub BackgroundWorker2_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker2.DoWork
ResetConnection:
        tl = New TcpListener(IPAddress.Any, CInt(My.Settings.LocalPort))
        tl.Start()

        soc = tl.AcceptSocket

        ns = New NetworkStream(soc)
        br = New BinaryReader(ns)

        While True
            Try
                Dim value As String = br.ReadString
                ds.dtAntrian.NewRow()
                ds.dtAntrian.Rows.Add(value)
                Try
                    If Not BackgroundWorker3.IsBusy Then
                        BackgroundWorker3.RunWorkerAsync()
                    End If
                Catch ex As Exception
                End Try
            Catch ex As Exception
                soc.Close()
                tl.Stop()
                GoTo ResetConnection
            End Try
        End While
    End Sub
    Dim dt As New DataTable
    Private Sub BackgroundWorker3_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker3.DoWork
        Try
            While True
                dt = ds.dtAntrian.CopyToDataTable : ds.dtAntrian.Clear()
                For Each row In dt.Rows
                    Dim str() As String = row("Antrian").ToString.Split(";")

                    Console.WriteLine(row("Antrian").ToString)

                    Try
                        noUrut = str(0)
                    Catch ex As Exception
                        noUrut = "-"
                    End Try
                    Try
                        idPoli = ""
                        aliasPoli = ""
                        prefixPoli = ""

                        Dim dr() As DataRow = ds.M_POLI.Select(String.Format("IDPOLI = '{0}'", str(1)))
                        If dr.Length > 0 Then
                            'count = 0
                            idPoli = dr(0)("IDPOLI")
                            aliasPoli = dr(0)("ALIAS")
                            prefixPoli = dr(0)("PREFIXANTRIAN")

                            For Each lbl In labelControl
                                If lbl.Tag = prefixPoli Then
                                    lbl.Text = noUrut
                                    Exit For
                                End If
                            Next

                            ' Dimatikan karena statis
                            'Select Case prefixPoli
                            '    Case "A"
                            '        lblQueueKIA.Text = noUrut
                            '    Case "B"
                            '        lblQueueUmum.Text = noUrut
                            '    Case "C"
                            '        lblQueueGigi.Text = noUrut
                            'End Select
                        Else
                            lblQueuePendaftaran.Text = noUrut
                        End If

                        Dim bilangan As Integer = CInt(noUrut.Substring(1, noUrut.Length - 1))

                        'hasil &= noUrut.Substring(0, 1) & " " & Terbilang(i)
                        hasil = noUrut.Substring(0, 1) & " " & Terbilang(bilangan)

                        strTerbilang = hasil

                        lblCurrentQueue.Text = noUrut
                        lblCurrentPoli.Text = aliasPoli

                        Call Panggil_L4()
                    Catch ex As Exception
                        aliasPoli = "-"
                    End Try
                    lblCurrentQueue.Text = noUrut
                    lblCurrentPoli.Text = aliasPoli
                Next
                dt.Clear()
            End While
        Catch ex As Exception
        End Try
    End Sub
    'Dim count As Integer
    'Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
    '    If count < 7 Then
    '        Select Case prefixPoli
    '            Case "A"
    '                If lblQueueKIA.Visible Then
    '                    lblQueueKIA.Visible = False
    '                Else
    '                    lblQueueKIA.Visible = True
    '                End If
    '            Case "B"
    '                If lblQueueUmum.Visible Then
    '                    lblQueueUmum.Visible = False
    '                Else
    '                    lblQueueUmum.Visible = True
    '                End If
    '            Case "C"
    '                If lblQueueGigi.Visible Then
    '                    lblQueueGigi.Visible = False
    '                Else
    '                    lblQueueGigi.Visible = True
    '                End If
    '            Case Else
    '                If lblQueuePendaftaran.Visible Then
    '                    lblQueuePendaftaran.Visible = False
    '                Else
    '                    lblQueuePendaftaran.Visible = True
    '                End If
    '        End Select
    '        count += 1
    '    Else
    '        lblQueueKIA.ForeColor = System.Drawing.Color.Black
    '        lblQueueUmum.ForeColor = System.Drawing.Color.Black
    '        lblQueueGigi.ForeColor = System.Drawing.Color.Black
    '        lblQueuePendaftaran.ForeColor = System.Drawing.Color.Black
    '        Timer1.Stop()
    '    End If
    'End Sub
End Class